<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\BorrowedBook;

class StoreReturnedBookRequest extends FormRequest
{
    /**
     * DETERMINE IF USER IS AUTHORIZE TO MAKE REQUEST
     *
     * @return bool
     */
    
    public function authorize()
    {
        return true;
    }

    /**
     * GET THE VALIDATION RULES THAT APPLY TO THE REQUEST
     *
     * @return array
     */

    public function rules()
    {
        $borrowed = BorrowedBook::where('book_id', request()->get('book_id'))->where('patron_id', request()->get('patron_id'))->first();
            if (!empty($borrowed)) {
                $copies = $borrowed->copies;
            } else {
                $copies = request()->get('copies');
            }

        return [
            'book_id' => 'required|exists:borrowed_books,book_id',
            'copies' => 'required|numeric',
            'patron_id' => 'exists:borrowed_books,patron_id'
        ];
    }

    /**
     * GET THE ERROR MESSAGE FOR THE VALIDATION RULES
     *
     * @return array
     */

    public function message()
    {
        return [        
            'copies.required' => 'Copies is required.',
            'copies.numeric' => 'Invalid input. Copies must only be a number.',
            'patron_id.exists' => 'Patron does not exist'
        ];
    }

    //Display error message
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}