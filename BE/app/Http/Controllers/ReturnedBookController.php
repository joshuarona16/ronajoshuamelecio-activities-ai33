<?php

namespace App\Http\Controllers;

use app\Models\BorrowedBook;
use app\Models\ReturnedBook;
use Illuminate\Http\Request;

class ReturnedBookController extends Controller
{
    public function index(){
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());
    }

    public function store(Request $request){
        $borrowedbook = BorrowedBook::where([['book_id', $request->book_id], ['patron_id', $request->patron_id,]])->firstOrFail();
    }

    public function show($id){
        return response()->json(ReturnedBook::with(['book', 'book.category', 'patron'])->findOrFail($id));
    }
}
