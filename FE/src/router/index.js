import Vue from 'vue';
import Router from 'vue-router';
import Header from '@/components/Header'
import Sidenav from '@/components/Sidenav'
import Dashboard from '@/pages/Dashboard'
import PatronManagement from '@/pages/PatronManagement'
import BookManagement from '@/pages/BookManagement'
import Settings from '@/pages/Settings'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/header',
      name: 'Header',
      component: Header,
      props: { page: 0 }
    },
    {
      path: '/sidenav',
      name: 'Sidenav',
      component: Sidenav,
      props: { page: 1 },
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      props: { page: 2 },
      alias: '/'
    },
    {
      path: '/patronmanagement',
      name: 'PatronManagement',
      props: { page: 3 },
      component: PatronManagement
    },
    {
      path: '/bookmanagement',
      name: 'BookManagement',
      props: { page: 4 },
      component: BookManagement
    },
    {
      path: '/settings',
      name: 'Settings',
      props: { page: 5 },
      component: Settings
    }
  ]
})