<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $categories=['Technologies', 'Romance', 'Fictions'];

        foreach($categories as $category){
                categories::create(['category'=>$category]);
        }
    }
}
