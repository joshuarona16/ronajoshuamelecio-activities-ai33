export default {
    data() {
   return {
     Patron: {
        PMFname: "",
        PMMname: "",
        PMLname: "",
        PMEaddress: "",
       PatronDateRegistered: new Date(),
     },
     PatronModel: [],
     SelectedPatron: "",
     Search: "",
   };
 },
 methods: {
   AddPatron() {
     this.PatronModel.unshift(this.Form);
     console.log(this.form);
     this.HideModal();
   },
   UpdatePatron(index) {
     console.log(index);
     this.PatronModel.splice(index, 1, this.Form);

     this.HideModal();
   },
   ToBeUpdated(index) {
     this.SelectedPatron = index;
     this.Form.FirstName = this.PatronModel[index].FirstName;
     this.Form.MiddleName = this.PatronModel[index].MiddleName;
     this.Form.LastName = this.PatronModel[index].LastName;
     this.Form.EmailAddress = this.PatronModel[index].EmailAddress;
   },
   DeletePatron(index) {
     this.PatronModel.splice(index, 1);

     this.HideModal();
   },
   HideModal() {
     this.$refs.modal.hide();
     this.FormReset();
   },
   FormReset() {
     this.Patron = {
        PMFname: "",
        PMMname: "",
        PMLname: "",
        PMEaddress: "",
        PatronDateRegistered: new Date(),
     };

     this.SelectedPatron = "";
   },
 },
 computed: {
   PatronModelFilter() {
     return this.PatronModel.filter((Patron) => {
       return this.Search.toLowerCase()
         .split(" ")
         .every((v) => Patron.PMFname.toLowerCase().includes(v));
     });
   },
 },
};
