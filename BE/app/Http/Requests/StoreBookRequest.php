<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Request;
use App\Models\BorrowedBook;

class StoreBookRequest extends FormRequest
{
    /**
     * DETERMINE IF USER IS AUTHORIZE TO MAKE REQUEST
     *
     * @return bool
     */
    
    public function authorize()
    {
        return true;
    }

    /**
     * GET THE VALIDATION RULES THAT APPLY TO THE REQUEST
    
     * @return array
     */

    public function rules()
    {
        return [        
            'name' => 'required|min:2|max:100', //unique:books,name
            'author' => 'required|min:1|max:5',
            'copies' => 'required|numeric',
            'category_id' => 'required|exists:categories,id'  
        ];
    }

    /**
     * GET THE ERROR MESSAGE FOR THE VALIDATION RULES
     *
     * @return array
     */
    
    public function message()
    {
        return [        
            'name.required' => 'Name is required.',
            'name.min' => 'Name must have at least minimum of 2 characters',
            'name.max' => 'Name must not exceed 100 maximum of characters',
            'autor.required' => 'Author is required',
            'author.min' => 'Author must have at least minimum of 2 characters',
            'author.max' => 'Author must not exceed 100 maximum of characters',
            'copies.required' => 'Copies is required.',
            'copies.numeric' => 'Invilid input. Copies must only be a number.',
            'category_id.required' => "Book must belong to a category",
            'category_id.exists' => "Category does not exist"
        ];
    }

    //DISPLAY ERROR MESSAGE
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}