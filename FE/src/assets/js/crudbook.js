export default {
    data() {
   return {
     Book: {
       name: "",
       author: "",
       copies: "",
       category: "",
       BookDateRegistered: new Date(),
     },
     BookModel: [],
     SelectedBook: "",
     Search: "",
   };
 },
 methods: {
   AddBook() {
     this.BookModel.unshift(this.Form);
     console.log(this.form);
     this.HideModal();
   },
   UpdateBook(index) {
     console.log(index);
     this.BookModel.splice(index, 1, this.Form);

     this.HideModal();
   },
   ToBeUpdated(index) {
     this.SelectedBook = index;
     this.name = this.BookModel[index].name;
     this.author = this.BookModel[index].author;
     this.copies = this.BookModel[index].copies;
     this.category = this.BookModel[index].category;
   },
   DeleteBook(index) {
     this.BookModel.splice(index, 1);

     this.HideModal();
   },
   HideModal() {
     this.$refs.modal.hide();
     this.FormReset();
   },
   FormReset() {
     this.Form = {
       name: "",
       author: "",
       copies: "",
       category: "",
       BookDateRegistered: new Date(),
     };

     this.SelectedBook = "";
   },
 },
 computed: {
   BookModelFilter() {
     return this.BookModel.filter((Book) => {
       return this.Search.toLowerCase()
         .split(" ")
         .every((v) => Book.name.toLowerCase().includes(v));
     });
   },
 },
};
